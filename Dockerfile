FROM archlinux:latest
ENV PATH="/root/.local/bin:/root/.cargo/bin/:${PATH}"
ENV GIT_DEPTH=$GIT_DEPTH
ENV GIT_SUBMODULE_STRATEGY=$GIT_SUBMODULE_STRATEGY
ENV TERM=linux
RUN pacman -Syy \
  && pacman -S --needed --noconfirm reflector \
  && reflector --verbose --age 24 --connection-timeout 3 --download-timeout 3 --protocol https --sort rate --threads 4 --save /etc/pacman.d/mirrorlist \
  && pacman -Syu --noconfirm \
  && pacman -S --needed --noconfirm \
  archiso \
  archlinux-keyring \
  base \
  base-devel \
  erofs-utils \
  git \
  git-lfs \
  gnupg \
  jq \
  p7zip \
  pacman-contrib \
  pacutils \
  python \
  python-black \
  python-coverage \
  python-gnupg \
  python-packaging \
  python-pip \
  python-psutil \
  python-pytest \
  python-pytest-black \
  python-pytest-cov \
  python-pytest-flake8 \
  python-setuptools \
  reflector \
  shellcheck \
  tree \
  util-linux \
  wget \
  yajl \
  && pacman -Scc \
  && pacman-key --init \
  && pacman-key --populate archlinux \
  && git lfs install \
  && useradd -m builder \
  && echo "builder   ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers \
  && yes | pacman-key --recv-key 3056513887B78AEB --keyserver keyserver.ubuntu.com \
  && yes | pacman-key --lsign-key 3056513887B78AEB \
  && pacman -U --noconfirm 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst' \
  && echo -e "[chaotic-aur]\nInclude = /etc/pacman.d/chaotic-mirrorlist" >> /etc/pacman.conf \
  && pacman -Syy && pacman -S --noconfirm yay \
  && reflector --verbose --age 24 --connection-timeout 3 --download-timeout 3 --protocol https --sort rate --threads 4 --save /etc/pacman.d/mirrorlist
CMD ["bash"]
